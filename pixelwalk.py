#for loops

from scipy import misc
import matplotlib.pyplot as plt
from numpy import random
a = misc.imread('grains.png',flatten=True)
print(a.max(),a.min())
a[a>128]=255
a[a<=128]=0
plt.imshow(a)
plt.show()

#generate random points
def randpoint(array):
	x = randint(array.shape[1])
	y = randint(array.shape[0])
	return array[y,x]

#stepping examples
#this code doesn't include error-checking for the edges of our array
#the row and column can be used in  'image[row,column]'
row,column = 3,14
#to step one 'east'
row,column = row,column+1
#to step one 'west
row,column = row,column-1
#to step one 'north'
row,column = row+1,column
#to step one 'south'
row,column = row-1,column



